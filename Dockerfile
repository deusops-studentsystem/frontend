FROM node:16-alpine as builder
WORKDIR /app
COPY . .
ARG BACKEND_API_HOST
ENV BACKEND_API_HOST=${BACKEND_API_HOST}
RUN sed -i "s/BACKEND_API_VALUE/$BACKEND_API_HOST/g" ./src/components/Student.js
RUN npm i
RUN npm run build

FROM nginx
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
ENTRYPOINT ["nginx", "-g", "daemon off;"]